import json
from ebooklib import epub
from book_summarizer.parse import parse_epub
from book_summarizer.node import Node


if __name__ == "__main__":
    book = epub.read_epub("/home/arthur/Downloads/Jens_Amberts_Why_an_Afterlife_Obviously_Exists_A_Thought_Experiment.epub")
    book_tree, graph = parse_epub(book)
    graph.render("book_graph")
    tree = Node.from_dict(book_tree)
    try:
        tree.summarize()
    except Exception as e:
        print(f"Error: {e}")

    # write tree.json() to file
    with open("book_summary.json", "w") as f:
        json.dump(tree.json(), f, indent=4)

