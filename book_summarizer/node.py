from tqdm import tqdm
from .summarizer import summarize


class Node:

    def __init__(self, parent=None):
        self.parent = parent
        self.title = ""
        self.summary = ""
        self.children = []

    @classmethod
    def from_dict(cls, d, parent=None):
        node = cls(parent=parent)
        if isinstance(d, dict):
            node.title = list(d.keys())[0]
            node.children = [Node.from_dict(child, parent=node) for child in list(d.values())[0]]
        else:
            node.title = "paragraph"
            node.summary = ""
            sub_node = cls(parent=node)
            sub_node.title = d
            node.children = [sub_node]
        return node
    
    @property
    def previous_summary(self):
        if self.parent is None:
            return ""
        summary = self.parent.previous_summary
        for sibling in self.parent.children:
            if sibling == self:
                return summary
            summary += sibling.summary + " "
        raise ValueError("This node is not a child of its parent")
    
    def summarize(self):
        if self.summary:
            return self.summary
        if not self.children:
            self.summary = self.title
        else:
            gen = tqdm(self.children) if len(self.children) > 1 else self.children
            if len(self.children) > 1:
                print(f"\nSummarizing {self.title}...")
            texts = [child.summarize() for child in gen]
            self.summary = summarize(texts)  # , context=self.previous_summary)
        return self.summary

    def __str__(self):
        return f"{self.title}: {self.summary}"
    
    def json(self):
        if not self.children:
            return self.summary
        return {"summary": self.summary, self.title: [child.json() for child in self.children]}
    