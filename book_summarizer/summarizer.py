"""
The Summerizer take the recursive book tree from parse.py and creates a summary of the book recursively. 
The result should be a tree of the book, where each node contains a summary of all the text below it.
Summaries are generated by calling the function summarize(text, context), where the context consists of a 
concatenation of the hightest level summaries generated so far.
"""

import os
import openai
from dotenv import load_dotenv

# ROLE = """
# You are given a context and a paragraph. 
# Your task is to summarize the paragraph so that the summary makes sense in the context.
# If you can't complete the task, answer with the exact word 'FAIL'.
# """
ROLE = """
Your task is to summarize the text. If you can't complete the task, answer with the exact word 'FAIL'.
"""

load_dotenv(verbose=True)
openai.api_key = os.getenv("OPENAI_API_KEY")
model_engine = os.getenv("OPENAI_MODEL_ENGINE")


def ask_chat_gpt(text: str, role: str) -> str:
    messages = [{'role': 'system', 'content': role}, {'role': 'user', 'content': text}]
    response = openai.ChatCompletion.create(
        model=model_engine,
        messages=messages,
    )
    content = response['choices'][0]['message']['content']
    return content


def summarize(texts: list, context: str="", depth: int=0) -> str:
    # content = f"CONTEXT:\n{context}\n\nPARAGRAPH:\n{text}"
    if len(texts) == 1 and depth > 0:
        return texts[0]
    summaries = []
    for sub_texts in divide_in_chunks(texts):
        joined = " ".join(sub_texts)
        response = ask_chat_gpt(joined, ROLE)
        if "FAIL" in response:
            if len(joined) < 1000:
                summaries.append(joined)
        else:
            summaries.append(response)
    return summarize(summaries, context=context, depth=depth+1)

    # response = openai.ChatCompletion.create(
    #     engine=model_engine,
    #     prompt=content,
    #     temperature=0.3,
    #     max_tokens=50,
    #     top_p=1,
    #     frequency_penalty=0.5,
    #     presence_penalty=0.5,
    #     stop=["\n", " [", "."]
    # )
    # summary = response['choices'][0]['text']
    # return summary


def divide_in_chunks(texts: list, MAX_LEN:int=16000):
    chunk = []
    for text in texts:
        if len(" ".join(chunk)) + len(text) > MAX_LEN:
            yield chunk
            chunk = []
        chunk.append(text)
    yield chunk


def concatenate(texts: list, MAX_LEN:int=16000) -> str:
    text = ""
    for t in texts[::-1]:
        if len(text) + len(t) > MAX_LEN:
            break
        text = t if text == "" else t + " " + text
    return text