from ebooklib import epub, ITEM_DOCUMENT
from bs4 import BeautifulSoup
from graphviz import Digraph


def parse_epub(book):
    chapters = []
    graph = Digraph()
    graph.format = "pdf"
    graph.attr("node", shape="rectangle")
    for k, item in enumerate(book.get_items()):
        if item.get_type() != ITEM_DOCUMENT:
            continue
        soup = BeautifulSoup(item.get_content(), "html.parser")
        if soup.title:
            chapter_title = soup.title.string.strip()
        else:
            chapter_title = "No title"
        chapter_contents = parse_html(soup.body, graph, parent="book title")
        chapters.extend(_get_content(chapter_title, chapter_contents, skip_strings=True))
    return {"Book": chapters}, graph


def parse_html(tag, graph, parent=None):
    contents = []
    for child in tag.children:
        if child.name in ["p", "h1", "h2", "h3", "h4", "h5", "h6", "blockquote"]:
            text = child.text.strip() 
            if text == parent:
                continue           
            # if parent:
            #     graph.node(text[:10])
            #     graph.edge(parent, text[:10])
            contents.append(text)
        elif child.name == "section":
            section_title = "No title"
            if child.find("h1"):
                section_title = child.find("h1").text.strip() 
                graph.node(section_title, shape="folder")
                if parent:
                    graph.edge(parent, section_title)
            section_contents = parse_html(child, graph, parent=section_title)
            contents.extend(_get_content(section_title, section_contents))
    return contents


def _get_content(title, contents, skip_strings=False):
    if not contents:
        return []
    if isinstance(contents, list) and contents[0] == "":
        return []
    if title == "No title":
        return [c for c in contents if not isinstance(c, str) or not skip_strings]
    else:
        return [{title: contents}]
